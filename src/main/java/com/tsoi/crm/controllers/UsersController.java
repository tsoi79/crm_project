package com.tsoi.crm.controllers;

import com.tsoi.crm.forms.UserForm;
import com.tsoi.crm.models.User;
import com.tsoi.crm.services.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class UsersController {
    private final UsersService usersService;

    @Autowired
    public UsersController(UsersService usersService) {
        this.usersService = usersService;
    }

    // -----------------GET_REQUESTS---------------
    @GetMapping("/admin")
    public String dashBoard() {
        return "admin";
    }

    @GetMapping("/users")
    public String getUsersData(Model model) {
        List<User> users = usersService.getAllUsers();
        model.addAttribute("users", users);
        return "users";
    }

    @GetMapping("/users/{user-id}")
    public String getUserPage(Model model, @PathVariable("user-id") Integer id) {
        User user = usersService.getUserById(id);
        model.addAttribute("user", user);
        return "/user";
    }

    @GetMapping("/register")
    public String register() {
        return "redirect:/register.html";
    }

    @GetMapping("/new_user")
    public String newUser() {
        return "redirect:/add_user.html";
    }

    // -----------------POST_USER_REQUESTS---------------
    @PostMapping("/self_reg")
    public String selfRegistration(UserForm form) {
        usersService.register(form);
        return "redirect:login.html";
    }
    @PostMapping("/login")
    public String login(@RequestParam("email") String email,
                        @RequestParam("пароль") String password) {
        System.out.println(email + " " + password);
        return "redirect:/personal.html";
    }

    // -----------------POST_ADMIN_REQUESTS---------------
    @PostMapping("/add_user")
    public String addUser(UserForm form) {
        usersService.add(form);
        return "redirect:/users";
    }

    @PostMapping("/users/{user-id}/delete")
    public String removeUser(@PathVariable("user-id") Integer id) {
        usersService.remove(id);
        return "redirect:/users";
    }

    @PostMapping("/users/{user-id}/change")
    public String changeUserData(UserForm form,  @PathVariable("user-id") Integer id) {
        usersService.change(form, id);
        return "redirect:/users";
    }
}
