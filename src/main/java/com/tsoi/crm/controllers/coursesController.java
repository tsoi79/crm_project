package com.tsoi.crm.controllers;

import com.tsoi.crm.models.Course;
import com.tsoi.crm.models.User;
import com.tsoi.crm.services.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller
public class coursesController {


    private final CourseService courseService;

    @Autowired
    public coursesController(CourseService courseService) {
        this.courseService = courseService;
    }

    @GetMapping("/courses")
    public String getCourseData(Model model) {
        List<Course> courses = courseService.getAllCourses();
        model.addAttribute("courses", courses);
        return "courses";
    }

    @GetMapping("/courses_all")
    public String getCourseDataForAll(Model model) {
        List<Course> courses = courseService.getAllCourses();
        model.addAttribute("courses", courses);
        return "courses_all";
    }

}
