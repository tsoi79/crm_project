package com.tsoi.crm.repositories;

import com.tsoi.crm.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface UsersRepository extends JpaRepository<User, Integer> {

    List<User> findAllByUserSurname(String surname);

    List<User> findAllByNumberOfPaidLessons(Integer lessons);

}
