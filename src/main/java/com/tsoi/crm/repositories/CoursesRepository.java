package com.tsoi.crm.repositories;

import com.tsoi.crm.models.Course;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CoursesRepository extends JpaRepository<Course, Integer> {

}
