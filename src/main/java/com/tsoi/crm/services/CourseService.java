package com.tsoi.crm.services;

import com.tsoi.crm.models.Course;
import com.tsoi.crm.models.User;

import java.util.List;

public interface CourseService {
    List<Course> getAllCourses();
}
