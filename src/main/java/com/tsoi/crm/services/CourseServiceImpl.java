package com.tsoi.crm.services;

import com.tsoi.crm.models.Course;
import com.tsoi.crm.repositories.CoursesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Component
public class CourseServiceImpl implements CourseService {

    private final CoursesRepository coursesRepository;

    @Autowired
    public CourseServiceImpl(CoursesRepository coursesRepository) {
        this.coursesRepository = coursesRepository;
    }

    @Override
    public List<Course> getAllCourses() {
        return coursesRepository.findAll();
    }
}
