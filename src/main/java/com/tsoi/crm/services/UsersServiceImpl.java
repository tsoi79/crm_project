package com.tsoi.crm.services;

import com.tsoi.crm.forms.UserForm;
import com.tsoi.crm.models.User;
import com.tsoi.crm.repositories.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class UsersServiceImpl implements UsersService {

    private final UsersRepository usersRepository;
    @Autowired
    public UsersServiceImpl(UsersRepository usersRepository) {
        this.usersRepository = usersRepository;
    }

    @Override
    public void add(UserForm form) {
        User user = User.builder().userName(form.getName())
                .userSurname(form.getSurname())
                .email(form.getEmail())
                .hashPassword(form.getPassword())
                .numberOfPaidLessons(form.getLessons())
                .build();
        usersRepository.save(user);
    }

    @Override
    public List<User> getAllUsers() {
        return usersRepository.findAll();
    }

    @Override
    public void remove(Integer id) {
        usersRepository.deleteById(id);
    }

    @Override
    public User getUserById(Integer id) {
        return usersRepository.getById(id);
    }

    @Override
    public List<User> findAllByNumberOfPaidLessons(Integer lessons) {
        return usersRepository.findAllByNumberOfPaidLessons(lessons);
    }

    @Override
    public List<User> findAllBySurname(String surname) {
        return usersRepository.findAllByUserSurname(surname);
    }

    @Override
    public void register(UserForm form) {
        User user = User.builder().userName(form.getName())
                .userSurname(form.getSurname())
                .email(form.getEmail())
                .hashPassword(form.getPassword())
                .numberOfPaidLessons(0)
                .build();
        usersRepository.save(user);
    }

    @Override
    public void change(UserForm form, Integer id) {
        User user = usersRepository.getById(id);
        user.setUserName(form.getName());
        user.setUserSurname(form.getSurname());
        user.setNumberOfPaidLessons(form.getLessons());
        usersRepository.save(user);
    }

}
