package com.tsoi.crm.services;

import com.tsoi.crm.forms.UserForm;
import com.tsoi.crm.models.User;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface UsersService {
    void add(UserForm form);
    void remove(Integer id);
    User getUserById(Integer id);
    List<User> getAllUsers();
    List<User> findAllByNumberOfPaidLessons(Integer lessons);
    List<User> findAllBySurname(String surname);

    void register(UserForm form);

    void change(UserForm form, Integer id);
}
