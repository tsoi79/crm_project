create table users
(
    id                     serial primary key,
    user_name              varchar(20),
    user_surname           varchar(30),
    email                  varchar(20),
    password               varchar(30),
    number_of_paid_lessons integer
);

create table courses
(
    id                 serial primary key,
    course_title       varchar(100),
    course_description varchar,
    course_volume      integer,
    course_price       integer
);

create table users_courses
(
    id        serial primary key,
    user_id   integer,
    course_id integer,
    foreign key (user_id) references users (id),
    foreign key (course_id) references courses (id)
);

insert into courses(course_title, course_description, course_volume, course_price)
values ('Программирование игр на Scratch', 'Курс для детей 10-12 лет. Каждый станет разработчиком собственной игры', 24,
        12000);
insert into courses(course_title, course_description, course_volume, course_price)
values ('3D-моделирование в Blender', 'Курс для детей 12-15 лет. Основы работы в 3d редакторе Blender', 48, 24000);
insert into courses(course_title, course_description, course_volume, course_price)
values ('Робототехника 1 уровень', 'Курс для детей 8-11 лет. Обучение конструированию и основам программирования', 48,
        24000);
insert into courses(course_title, course_description, course_volume, course_price)
values ('Робототехника 2 уровень', 'Продолжение курса "Робототехника"', 24, 12000);
insert into courses(course_title, course_description, course_volume, course_price)
values ('Основы электроники 1 уровень',
        'Курс для детей 12-15 лет. Изучение основ электротехники, электроники и программирования на языке С++', 48,
        24000);
insert into courses(course_title, course_description, course_volume, course_price)
values ('Основы электроники 2 уровень', 'Продолжение курса "Основы электроники"', 48, 24000);
insert into courses(course_title, course_description, course_volume, course_price)
values ('Алгоритмы и структуры данных',
        'Курс для подростков 14-16 лет. Изучение алгоритмов и структур данных (программирование на языке Python)', 48,
        24000);